# soal-shift-sisop-modul-1-ITB07-2022

## Laporan Resmi Modul 1 Praktikum Sistem Operasi 2022
---
### Kelompok ITB07:
Calvindra Laksmono Kumoro - 5027201020  
Afrida Rohmatin Nuriyah - 5027201037  
Brilianti Puspita Sari - 5027201070  

---
## Soal 1
## Soal 1a
## Analisa soal 1a:
Pada nomor 1 pertama han diminta untuk membuat program register dan login yang sudah ditentukan dari soal.

## Cara pengerjaan 1a:
Pada soal ini diminta untuk membuat script register.sh, main.sh dan file disimpan dalam ./users/user.txt
![user.txt](./img/1a.JPG)

## Kendala 1a: 
Untuk kendala pada nomor 1a belum ada, karena berupa perintah yang sangat basic dan mudah dipahami


## Soal 1b
## Analisa Soal 1b:
Untuk program register ini dimulai dengan read agar nantinya user bisa menginput username dan password yang diinginkan. Kemudian dalam program menggunakan fungsi check dimana nanti mengecek sesuai setiap persyaratan yang ada dari soal. Persyaratan-persyaratan itu terdiri dari:
 1. Minimal 8 karakter
 2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil 
 3. Mengandung Alphanumeric
 4. Password idak boleh sama dengan username

## Cara pengerjaan 1b:
Dan pada program ini mengecek mulai dari apakah username sudah masuk pada file user.txt atau belum, dan untuk mengeceknya saya menggunakan egrep untuk bisa melihat variabel username yang sudah ditentukan pada user.txt, dan jika memenuhi persayaratan if egrep tersebut maka program akan menampilkan output REGISTER: ERROR User already exists. 		
	Untuk pengecekan selanjutnya akan mengecek beberapa persyaratan lainnya yaitu: jika program panjangnya tidak sama dengan 8 karakter, tidak memiliki huruf kecil, tidak memiliki huruf besar, tidak mengandung alpahnumeric dan password tidak boleh sama dengan username. Jadi ketika inputan dari user memnuhi dari persyaratan yang ada maka akan masuk pada statement yang ada sehingga akan merunning perintah untuk password akan dikosongkan nilainya dengan algoritma password=0, kemudian user akan diminta kembali memasukkan password yang benar, dan akan dilakukan check kembali karena saya menggunakan fungsi ini kembali di dalam statement untuk melakukan pengecekan dan bisa melakukan looping sampai inputan dari user benar.

## Source Code 1b:
```sh
elif [ ${#password} -lt 8 ] 
then
    echo $password " kurang memenuhi persyaratan minimal 8 karakter"
    password=0
    read -s -p "Masukkan kembali password yang benar:" password
    check
    
    

    
elif [ $password = ${password,,} ] 
then 
    echo $password " tidak memiliki 1 huruf besar"
    password=0
    read -s -p "Masukkan kembali password yang benar:" password
    check
    
    
 

elif [ $password = ${password^^} ]
then 
        echo $password " tidak memiliki 1 huruf kecil"
        password=0
    read -s -p "Masukkan kembali password yang benar:" password
    check
    
elif [[ $passsword =~ [^a-zA-Z0-9\] ]] 
then
    echo "password tidak mengandung alphanumeric"
    password=0
    read -s -p "Masukkan kembali password yang benar:" password
    check
    
elif [ $password == $username ]
then
    echo "password tidak boleh sama dengan username"
    password=0
    read -s -p "Masukkan kembali password yang benar:" password
    check
}

check
```
## Test Output 1b:
Disini kami mengetes dengan menggunakan beberapa password untuk menguji code persyaratan yang sudah dibuat:
1. Input Username: calvindro & Input password: Calv12
![user.txt](./img/lt8.JPG)
2. Input Username: calvindrlk & Input password: calvindra
![user.txt](./img/besar.JPG)
3. Input Username: calvinlk & Input password: CALVINDRALK
![user.txt](./img/kecil.JPG)
4. Input Username: Calvindra123 & Input password: Calvindra123
![user.txt](./img/same.JPG)

## Kendala 1b:
Kendala yang kami alami ketika mengerjakan soal nomor 1b ini yaitu pada bagian alphanumeric. Persyaratan alphanumeric terkadang tidak bekerja. Dan kesalahan lain seperti penggunaan spasi yang tidak tepat sehingga membuat program yang dibuat menjadi error.

## Soal 1c
## Analisa Soal 1c:
Pada soal nomor 1c kita diminta agar semua percobaan register dan login bisa tercatat dalam log.txt dan setiap percobaan akan menampilkan MM/DD/YY di dalam log.txt

## Cara Pengerjaan 1c:
Untuk bisa menampilkan dari MM/DD/YY hh:mm:ss disini saya menggunakan syntax tgl=$(date), jadi nantinya ketika ingin menampilkan, diperlukan untuk memanggilnya dengan $tgl. Dan semua aksi tersebut akan tersimpan dalam log.txt untuk memantau dari setiap inputan user apakah berhasil atau salah.

```sh
tgl=$(date)

echo "Silahkan login di ITB07:"
read -p "Masukkan username:" username    
read  -s -p "Masukkan  password:" password
```
Kemudian pada nomor ini diminta bahwa ketika username sudah terdaftar maka akan menampilkan ERROR User already exist pada file log.txt. 
  ![user.txt](./img/exist.JPG)
Dan perintah tersebut terdapat pada register.sh dan program akan melakukan pengecekan dari egrep tersebut. Dimana natinya egrep akan mengecek apakah username sudah ada atau belum dari file user.txt yang ada, tapi jika tidak ada maka akan lanjut pada pengecekan selanjutnya. 
```sh
if egrep "$username" users/user.txt > users/temp.txt #egrep digunakan apakah sudah ada apa belum usernamenya
then
	echo "REGISTER: ERROR $username Already Exist" >>users/log.txt
```

Kemudian program juga akan menampilkan REGISTER: INFO User USERNAME registered successfully apabila semua pengecekan dari fungsi check sudah berhasil dan masuk pada else.
  ![user.txt](./img/registered.JPG)
Pada login ketika user memasukkan password yang salah akan menampilkan LOGIN: ERROR Failed login attempt on user USERNAME pada file log.txt dan ketika berhasil login akan menampilkan juga LOGIN: INFO User USERNAME logged in pada file log.txt. 
 ![user.txt](./img/logfail.JPG)
Semua persyaratan itu  menggunakan awk untuk melakukan pengecekan dari file user.txt dengan mengecek dari field $2 yang berisi username dan $5 yang berisi password. dan ketika input yang dilakukan user itu sesuai dan benar maka akan mengprint  1 dan bila tidak, tidak memprint angka 1. Kemudian setelah memmprint program akan masuk pada kondisi until dimana nantinya apabila output 1 tersebut sama dengan 1 maka user berhasil login. Dan ketika yang diprint bukan angka 1 maka akan masuk pada looping, kemudian melakukan input username dan password kembali, dan akan dilakukan pengecekan kembali menggunakan awk. Dan looping akan berhenti jika angka 1 berhasil diprint dari pengecekan awk tersebut.
```sh
read -p "Masukkan username:" username    
read  -s -p "Masukkan  password:" password

exists=$(awk -v username="${username}" -v password="${password}" '$2==username && $5==password {print 1}' ./users/user.txt)


until [[ "$exists" -eq 1 ]];
do
    echo " $tgl LOGIN: ERROR failed login attempt on user $username" >>users/log.txt
    read -p "Masukkan username:" username
    read  -s -p "Masukkan  password:" password
    exists=$(awk -v username="${username}" -v password="${password}" '$2==username && $5==password {print 1}' ./users/user.txt)    
done

echo "$tgl LOGIN: INFO User $username logged in" >>users/log.txt
```

## Isi file user.txt:
![user.txt](./img/user.JPG)

## Kendala 1c:
Kendala yang kami temui pada nomor ini yaitu penggunaan AWk yang termasuk sesuatu hal yang baru, dan membuat setiap kami perlu mempelajari cara penggunaan agar bisa memudahkan dalam pengerjan kode program.

## Soal 1d 
## Analisa soal 1d:
Pada soal 1d ini diminta agar nantinya user bisa memasukkan 2 jenis command yaitu, dl dan att. Dimana command dl digunakan untuk mengunduh jumlah gambar yang diinginkan oleh user, dan command att digunakan untuk menghitung jumlah percobaan user melakukan login  berhasil dan tidak.

## Cara Pengerjaan 1d:
Program ini akan bekerja setelah user berhasil melakukan login atau keluar dari looping karena username dan password yang dimasukkan benar. Kemudian user diminta menginput command antara dl dan att. Dan pada command att nantinya bisa menghitung jumlah total login dengan algoritma:
```sh
if [ $command == 'att' ]
    then
        failcount=$(egrep -c "$tgl LOGIN: ERROR failed login attempt on user $username" ./users/log.txt)
        successcount=$(egrep -c "$tgl LOGIN: INFO User $username logged in" ./users/log.txt)
        total=$(($failcount + $successcount))
        echo "User $username has attempted to login for $total times"
```
Untuk bisa menghitung jumlah pecobaan, kami menggunakan egrep untuk bisa mengecek dari file log.txt untuk dilakukan penghitungan. dan total penghitungan didapat melalui jumlah percobaan login yang berhasil dan percobaan login yang gagal.
![user.txt](./img/att.JPG)
Kemudian jika user memilih command dl, maka algoritma program akan mencetak waktu user ketika memilih command tersebut dalam program, setelah itu akan membuat directory berupa folder dengan format nama YYYY-MM-DD_USERNAME. Dan untuk memperoleh format nama folder dengan nama itu diperlukan mendeklarasikan dir="$time$username" . Kemudian program akan melanjutkan meng-unzip dari zip yang sudah dibuat. Kemudian program akan melooping sesuai dari jumlah dl yang diinginkan pengguna, dan setelah itu gambar berhasil terdownload dan masuk pada directory pengguna.
```sh
elif [ $command == 'dl' ]
    then
    time=`date +%Y-%m-%d_`
    printf $time
    dir="$time$username"
    if zipfile=$(ls | grep -o ".*${username}.zip")
    then
        unzip $zipfile
        dir=${zipfile::-4}
    else
        dir="$time$username"
        mkdir $dir
    fi
    count=$(ls $dir |  grep -c "PIC") 
    for ((i=count+1; i<=count+n; i=i+1))
    do
    wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
    done
```
Berikut gambar zip yang berhasil dibuat, 
![user.txt](./img/zip.JPG)

## Kendala 1d:
Kendala yang kami alami ketika mengerjakan nomor 1d ini yaitu ketika bingung untuk membuat algoritma dalam pembuatan directory dan kemudian file zip yang sudah dibuat perlu diunzip.


---
## Soal 2
#  Soal 2
## Exe. 2a
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info 

Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.

![](img/img%2010.png)

### Analisa Soal
pada soal ini diminta untuk membuat sebuah folder sebagai tempat file-file yang akan dikerjakan setelah nya 

### Cara Pengerjaan
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)

``` bash
$ nano nama_file.sh
```
setelah membuat script awk dengan extension .sh, langkah selanjutnya yaitu membuat folder pada script tersebut 
```bash 
$ mkdir [namadirectory]
```
mkdir merupakan salah satu basic command linux yang memiliki arti (make directory). 

![](img/img%201.png)

### Kendala
Pada awalnya masih bingung dengan script .sh yang diminta, setelah melihat modul jadi paham maksud dari "menggunakan script .sh untuk pengerjaan nomor 2"


## Exe. 2b
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

![](img/img%203.png)

### Analisa Soal
Pada soal ini diminta untuk mencari rata-rata request per jam, didalam file .log tersebut terdapat jam request disetiap request, maka dalam 1 jam ada berapa request yang masuk dan hitung rata-ratanya.

Hasil dari rata-rata tersebut dimasukkan kedalam file bernama ratarata.txt di folder yang sudah dibuat.

### Cara Pengerjaan 
referensi
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)
- [stackoverflow](https://stackoverflow.com/questions/9176000/minimum-average-and-maximum-in-columns)

``` awk
awk -F: ' '
```
awk digunakan dengan membaca script per kolom, di sini kami input **-F:** sebagai tanda bahwa script tersebut dipisahkan dengan menggunakan **:** maka awk akan membaca dengan perhitungan pemisah ":"
``` awk
NR == 1 { min = max = $3 } { if ($3 > max) max = $3; else if ($3 < min) min = $3 }
```
berdasarkan sumber yang saya temukan di stackoverflow, mencari rata-rata per jam. **$3** mendeklarasikan jam setiap request. "min = max = $3" di sini min & max dibuat sama lalu dibuat kondisi dengan menggunakan if elseif jika $3 lebih dari maka ia akan dideklarasikan sebagai variabel max, jika kurang dari akan menjadi min. 

"$3" merupakan jam jika dari 01:00:00 kondisi min = max maka membaca $3 tidak akan sampai 02:00:00, karena total request per jam. 
``` awk
(NR-1)/(max-min+1)
```
"NR-1" merupakan tanda dimana proses membaca awk dimulai dari baris kedua dst. "NR-1/max-min+1" algoritma untuk menghitung rata-rata total request per jam nya. 
``` bash
log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt
```
syntax untuk menyambungkan sekaligus membuat file baru file & folder
![](img/img%202.png)

### Kendala
Pada awalnya dalam penggunaan awk masih terdapat kekeliruan dengan konsep pengerjaan sebuah awk, setelah melihat modul kami menjadi paham. Error terjadi saat pengerjaan algoritman perhitungan karena sempat hasilnya mengeluarkan output 0.0000 dimana algoritma/syntax perhitungan tidak terbaca. Setelah itu kami mencari pada website stackoverflow untuk menggunakan syntax yang berbeda lalu berhasil.


## Exe. 2c
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
![](img/img%203.png)

### Analisa Soal
Pada soal ini diminta untuk menampilkan IP yang paling banyak melakukan Request, pada file .log terdapat lebih dari 1 IP yang memiliki IP yang sama. maka dari itu kami diminta untuk mencari satu IP yang melakukan request paling banyak.

Hasil dari pengerjaan dimasukkan kedalam file result.txt di folder yang sama seperti sebelumnya.

### Cara Pengerjaan
referensi 
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)
- [Unix.com](https://www.unix.com/shell-programming-and-scripting/230523-gsub-function-awk.html)
``` awk
NR>1 { gsub(/"/, "") } { if ( x[$1]++ >= max ) max = x[$1] } END { for (i in x) if(max == x[i]) print ", i, "sebanyak", x[i], "requests\n" } 
```
**gsub(/"/, "")** fungsinya untuk mengembalikan jumlah subsititusi yang telah dibuat, jika variabel dalam file.log diubah maka seluruh input atau $0 akan digunakan.
``` awk
{ if ( x[$1]++ >= max ) max = x[$1] } END { for (i in x) if(max == x[i]) print ", i, "sebanyak", x[i], "requests\n" } 
```
sesuai dengan ketentuan soal, **$1** akan merepresentasikan sebagai IP dan di looping dengan jumlah seluruh IP yang melakukan requests, jika $1 lebih dari sama dengan max, dimana max sama dengan **x** yang merepresentasikan jumlah IP.
![](img/img%208.png)

### Kendala
Selama mengerjakan soal ini kendala ada di sumber yang ada di internet, karena kami cukup lama untuk mencari referensi dari soal tersebut. Error kami temui saat perhitungan syntax terdapat kekeliruan sehinggan output yang dihasilkan tidak ada


## Exe. 2d
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.

![](img/img%205.png)
### Analisa Soal
pada soal ini kami diminta untuk mencari request yang menggunakan user-agent curl, yaitu user-agent yang menggunakan curl untuk maksud tertentu.

### Cara Pengerjaan 
referensi 
- [Modul Sisop Github](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/blob/master/Modul1/README.md#15-variabel)
``` awk
awk '/curl/ {i++} END {print, i, }'
```
di sini kami menggunakan special rules dimana ada **BEGIN** dan **END** namun karena menurut kami BEGIN tidak terlalu dibutuhkan, maka kami hanya menggunakan END. 

END akan dieksekusi setu kali, setelah semua input dibaca. **/curl/** ini adalah per kondisian dimana kita ingin mencari user-agent yang menggunakan curl, maka output nanti adalah user-agent yang menggunakannya. i disini adalah looping karena input yang dibaca tidak hanya satu namun semua data yang ada di file.log

![](img/img%204.png)

### Kendala
Kendala dalam pengerjaan soal ini tidak terlalu sulit hanya perlu mengulas modul pada github, dan error juga tidak ditemukan selama pengerjaan


## Exe. 2e
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

![](img/img%207.png)

### Analisa soal
pada soal ini kami diminta untuk menghasilkan output yang melakukan request pada jam 2 tanggal 22, karena pada file.log terdapat banyak sekali request yang dilakukan dalam satuan jam.

Hasil dari output soal ini akan dimasukkan kedalam file result.txt

### Cara Pengerjaan
``` awk
awk -F: '{if($3=="02") print $1}'
```
Semua request pada website daffainfo dilakukan pada tanggal 22 Januari, yang harus dilakukan adalah buatlah awk dapat membaca jam dan buat kondisinya di jam 2.

**$3** akan membaca file.log dengan titik dua pemisah ketiga, dimana data tersebut merupakan jam dari IP melakukan request pada website. Di soal diminta untuk mengeluarkan output IP yang melakukan di Jam 2 maka buat lah kondisi dengan if **if($3=="02"** dimana $3 (JAM) sama dengan 02.

"print $1" output yang diminta merupakan IP user, maka awk akan membaca dan mencetak "$1" yang berisi IP pada file.log dari hasil pengkondisian if sebelumnya yang sudah dijelaskan.
![](img/img%206.png)

### Kendala
Kendala yang ditemukan tidak ada dan tidak ditemukan error


---
## Soal 3
**Analisa Soal**  
Pada soal ini diminta untuk memonitor ram dan size dari suatu directory dengan ketentuan:
1. Memonitor ram menggunakan command **free -m**
2. Memonitor size dari suatu directory menggunakan comand **du -sh <target_path>**
3. Target path yang akan dimonitor adalah **/home/user/**  

Kemudian langkah selanjutnya yaitu:  
a. Semua metrics dimasukkan ke dalam suatu file log bernama **metrics_{YmdHms}.log**, dengan {YmdHms} merupakan waktu di mana file script bash dijalankan  
b. Script yang digunakan untuk mencatat metrics tersebut akan berjalan otomatis setiap menit  
c. Lalu dibuat satu script untuk agregasi file log ke satuan jam. Script agregasi ini akan memiliki informasi dari file-file yang tergenerate setiap menit. File hasil agregasi berisi nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Penamaan untuk file hasil agregasi adalah **metrics_agg_{YmdH}.log**  
d. Semua file log hanya dapat dibaca oleh user pemilik file    


### Note
> Nama file untuk script per menit adalah `minute_log.sh`  
> Nama file untuk script agregasi per jam adalah `aggregate_minutes_to_hourly.sh`  
> Semua hasil file log diletakkan di `/home/user/log/`

### Isi dari file log hasil script yang dijalankan tiap menit
> mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

>15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

### Isi dari file aggregasi log hasil script yang dijalankan tiap jam
> type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

>minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M
maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M
average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62  

## Penyelesaian
### 3a
Pertama, mengetahui terlebih dahulu output dari command `free-m` dan `du -sh <target_path>`  
![image](./img/command.JPG)  

Kemudian, membuat file script `minute_log.sh` untuk mencatat metrics yang merupakan output dari command sebelumnya
```sh
#!/bin/bash

free1=$(free -m | awk '/Mem:/ {print $2","$3","$4","$5","$6","$7}')
free2=$(free -m | awk '/Swap:/ {print $2","$3","$4}')
disk=$(du -sh /home/afridarn/ | awk '{print $2","$1}')
```
Di sini, untuk mengambil nilai-nilai dari semua metrics digunakan 3 variabel yaitu `free1, free2, dan disk`. Pengisian variabel menggunakan **pipe**. Untuk variabel **free1**, output dari command free -m dijadikan input awk dengan criteria `/Mem:`, yang artinya awk akan mencari baris di mana string Mem dimuat, kemudian melakukan action `print $2","$3","$4","$5","$6","$7","` yang akan mencetak nilai metrics urut dari mem total hingga mem available.  

Lalu, untuk variabel **free2**, output dari command free -m dijadikan input awk dengan criteria `Swap:`, yang artinya awk akan mencari baris di mana string Swap dimuat, kemudian melakukan action `print $2","$3","$4`, yang akan mencetak nilai metrics urut dari swap total hingga swap free. Terakhir, untuk variabel **disk**, output dari comman du -sh /home/afridarn/ dijadikan input awk yang melakukan action `print $2","$1` sesuai dengan urutan yang diminta pada soal.  
```sh
echo $free1,$free2,$disk > /home/afridarn/log/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log
```
Setelah itu, kode di atas digunakan untuk mencetak ketiga variabel sebelumna dan dimasukkan ke dalam file `metrics_{YmdHms}.log` pada folder /home/afridarn/log
```sh
chmod 400 /home/afridarn/log/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log
```
Selanjutnya, karena pada 3d disebutkan bahwa file log bersifat sensitif maka file hanya dapat dibaca oleh user digunakan kode di atas untuk mengubah permission pada file log. `chmod 400` artinya file tersebut hanya dapat dibaca oleh user. 400 memiliki 3 digit, digit pertama untuk user sedangkan 4 memiliki fungsi untuk mengubah permission file menjadi read only, digit kedua untuk group dan digit ketiga untuk others sedangkan 0 memiliki fungsi mengubah permission file menjadi none.  
- Hasil dari bash minute_log.sh yang menghasilkan file log metrics
![image](./img/output_minute_log.sh.JPG) 
### 3b
Untuk menjalankan script minute_log.sh setiap menit dapat menggunakan **cron jobs**, diawali dengan mengetikkan command _crontab -e_ kemudian mengetikkan perintah crontab sebagai berikut:
```sh
* * * * * bash /home/afridarn/SisOp/modul1/soal3/minute_log.sh
```
Perintah di atas akan menjalankan script minute_log.sh setiap menit
- Hasil cron jobs
![image](./img/cronjobs1.JPG)
### 3c
Pertama, membuat file `aggregate_minutes_to_hourly_log.sh` yang berisi:
```sh
#!/bin/bash

cd /home/afridarn/log
cat metrics_2*.log> /home/afridarn/SisOp/modul1/soal3/result.txt
```
**cd /home/afridarn/log** digunakan untuk pindah ke directory di mana file log setiap menit disimpan. Setelah itu, `cat metrics_2*.log` digunakan untuk menampilkan semua file log yang penamaan file memuat string metrics_2, lalu hasil dari cat tersebut disimpan di file txt bernama result.txt yang nantinya file ini akan dihitung nilai minimal, maksimal, dan rata-rata dari setiap metrics  
```sh
#mencari nilai minimum setiap metrics
min_mem_total=$(awk -F, 'min=="" || $1 < min {min=$1} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_mem_used=$(awk -F, 'min=="" || $2 < min {min=$2} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_mem_free=$(awk -F, 'min=="" || $3 < min {min=$3} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_mem_shared=$(awk -F, 'min=="" || $4 < min {min=$4} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_mem_buff=$(awk -F, 'min=="" || $5 < min {min=$5} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_mem_available=$(awk -F, 'min=="" || $6 < min {min=$6} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_swap_total=$(awk -F, 'min=="" || $7 < min {min=$7} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_swap_used=$(awk -F, 'min=="" || $8 < min {min=$8} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_swap_free=$(awk -F, 'min=="" || $9 < min {min=$9} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)
min_path_size=$(awk -F, 'min=="" || $11 < min {min=$11} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)  
```
Kemudian, untuk mencari nilai minimum dari setiap metrics digunakan kode di atas. Untuk cara kerjanya kami ambil contoh pada metrics **mem total** : 

`min_mem_total=$(awk -F, 'min=="" || $1 < min {min=$1} END {print min}' /home/afridarn/SisOp/modul1/soal3/result.txt)` 

Di sini digunakan variabel **min_mem_total** untuk menampung nilai minimum dari mem total. Variabel ini diisi mennggunakan awk, `-F,` digunakan untuk memisahkan field berdasarkan tanda koma karena pada file result.txt yang merupakan agregasi file log setiap menit setiap nilai metrics dipisahkan oleh tanda koma. Lalu, awk akan memiliki criteria `min=="" || $1 < min` yang memiliki arti jika min belum memiliki nilai atau $1 kurang dari min maka akan melakukan action `min=$1` yang akan mengisi nilai dari min. Setelah semua baris telah dicek maka `print min` akan mencetak nilai dari min yaitu nilai terkecil dari $1 di semua baris. Untuk nilai metrics lainnya/variabel selanjutnya langkahnya juga sama demikian.  

```sh
#mencari nilai maksimum setiap metrics
max_mem_total=$(awk -F, 'max=="" || $1 > max {max=$1} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_mem_used=$(awk -F, 'max=="" || $2 > max {max=$2} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_mem_free=$(awk -F, 'max=="" || $3 > max {max=$3} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_mem_shared=$(awk -F, 'max=="" || $4 > max {max=$4} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_mem_buff=$(awk -F, 'max=="" || $5 > max {max=$5} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_mem_available=$(awk -F, 'max=="" || $6 > max {max=$6} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_swap_total=$(awk -F, 'max=="" || $7 > max {max=$7} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_swap_used=$(awk -F, 'max=="" || $8 > max {max=$8} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_swap_free=$(awk -F, 'max=="" || $9 > max {max=$9} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)
max_path_size=$(awk -F, 'max=="" || $11 > max {max=$11} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt) 
```
Kemudian, untuk mencari nilai maksimum dari setiap metrics digunakan kode di atas. Untuk cara kerjanya hampir sama seperti mencari nilai minimum, kami ambil contoh pada metrics **mem total** :  
`max_mem_total=$(awk -F, 'max=="" || $1 > max {max=$1} END {print max}' /home/afridarn/SisOp/modul1/soal3/result.txt)`
Di sini digunakan variabel **max_mem_total** untuk menampung nilai maksimum dari mem total. Variabel ini diisi mennggunakan awk, `-F,` digunakan untuk memisahkan field berdasarkan tanda koma karena pada file result.txt yang merupakan agregasi file log setiap menit setiap nilai metrics dipisahkan oleh tanda koma. Lalu, awk akan memiliki criteria `max=="" || $1 > max` yang memiliki arti jika max belum memiliki nilai atau $1 lebih dari max maka akan melakukan action `max=$1` yang akan mengisi nilai dari max. Setelah semua baris telah dicek maka `print max` akan mencetak nilai dari max yaitu nilai terbesar dari $1 di semua baris. Untuk nilai metrics lainnya/variabel selanjutnya langkahnya juga sama demikian.  

Selanjutnya, untuk mencari nilai rata-rata dari setiap metrics:
```sh
#mencari rata-rata dari setiap metrics
avg_mem_total=$(awk -F, '{ sum += $1 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_mem_used=$(awk -F, '{ sum += $2 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_mem_free=$(awk -F, '{ sum += $3 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_mem_shared=$(awk -F, '{ sum += $4 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_mem_buff=$(awk -F, '{ sum += $5 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_mem_available=$(awk -F, '{ sum += $6 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_swap_total=$(awk -F, '{ sum += $7 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_swap_used=$(awk -F, '{ sum += $8 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_swap_free=$(awk -F, '{ sum += $9 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
avg_path_size=$(awk -F, '{ sum += $11 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)
```  
Untuk cara kerja kode di atas digunakan contoh pada metrics **mem_total**:  
`avg_mem_total=$(awk -F, '{ sum += $1 } END { print sum / NR }' /home/afridarn/SisOp/modul1/soal3/result.txt)`  

Di sini digunakan variabel **avg_mem_total** untuk menampung nilai rata-rata dari mem total. Variabel ini diisi mennggunakan awk, `-F,` digunakan untuk memisahkan field berdasarkan tanda koma karena pada file result.txt yang merupakan agregasi file log tiap menit setiap nilai metrics dipisahkan oleh tanda koma. Lalu, awk akan melakukan action `sum += $1` yang akan menjumlahkan semua nilai $1 pada file result.txt. Lalu, setelah semua baris dicek akan dilakukan action `print sum / NR` yang akan mencetak nilai dari sum yang sebelumnya telah dihitung dibagi dengan **NR** yang merupakan total baris/total record dari file result.txt. **sum / NR** merupakan nilai rata-rata dan akan mengisi variabel avg_mem_total. Untuk nilai metrics lainnya/ variabel selanjutnya langkah kerjanya juga sama demikian.  

Kemudian, untuk mencetak semua nilai tersebut menggunakan command sebagai berikut:  
```sh
echo -e "minimum,$min_mem_total,$min_mem_used,$min_mem_free,$min_mem_shared,$min_mem_buff,$min_mem_available,$min_swap_total,$min_swap_used,$min_swap_free,/home/afridarn/,$min_path_size\nmaximum,$max_mem_total,$max_mem_used,$max_mem_free,$max_mem_shared,$max_mem_buff,$max_mem_available,$max_swap_total,$max_swap_used,$max_swap_free,/home/afridarn/,$max_path_size\naverage,$avg_mem_total,$avg_mem_used,$avg_mem_free,$avg_mem_shared,$avg_mem_buff,$avg_mem_available,$avg_swap_total,$avg_swap_used,$avg_swap_free,/home/afridarn/,$avg_path_size" > /home/afridarn/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
```  
`-e` pada echo digunakan untuk mencetak new line pada hasil yang akan dicetak. Kode di atas mencetak semua nilai metrics urut sesuai dengan yang ada pada soal. Kemudian hasilnya akan disimpan di folder log yang telah dibuat sebelumnya dengan penamaan yang telah ditentukan.  
  
Untuk memastikan bahwa file log hanya dapat dibaca oleh user maka ditambahkan kode sebagai berikut:  
```sh
#memastikan file log hanya dapat dibaca oleh user pemilik file
chown afridarn /home/afridarn/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
chmod 400 /home/afridarn/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
```  
`chown` digunakan untuk mengganti kepemilikan file. Awalnya hasil file aggregate log ini memiliki kepimilikan root, lalu digunakanlah **chown afridarn** yang akan mengganti kepimilikan file menjadi afridarn. `chmod 400` artinya file tersebut hanya dapat dibaca oleh user. 400 memiliki 3 digit, digit pertama untuk user sedangkan 4 memiliki fungsi untuk mengubah permission file menjadi read only, digit kedua untuk group dan digit ketiga untuk others sedangkan 0 memiliki fungsi mengubah permission file menjadi none.  
- Hasil dari bash aggregate_minutes_to_hourly_log.sh
![image](./img/output_aggregate.sh.JPG)

Untuk menjalankan script aggregate_minutes_to_hourly_log.sh setiap jam dapat menggunakan **cron jobs**, diawali dengan mengetikkan command _crontab -e_ kemudian mengetikkan perintah crontab sebagai berikut:
```sh
0 * * * * bash /home/afridarn/SisOp/modul1/soal3/aaggregate_minutes_to_hourly_log.sh
```
- Hasil dari cronjobs di atas
![image](./img/cronjobs2.JPG)
## 3d  
Seperti yang telah dijelaskan sebelumnya, untuk memastikan semua file log hanya dapat dibaca oleh user dapat menggunakan command berikut:  
- Pada file script minute_log.sh
```sh
chmod 400 /home/afridarn/log/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log
```
- Pada file aggregate_minute_to_hourly_log.sh
```sh
#memastikan file log hanya dapat dibaca oleh user pemilik file
chown afridarn /home/afridarn/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
chmod 400 /home/afridarn/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
```
- Bukti permission
![image](./img/readonly.JPG)  
### Kendala yang Dialami
Untuk nomor 3, kami sempat mengalami kendala dalam menemukan algoritma yang tepat untuk script aggregasi dan juga pada saat menjalankan cron jobs awalnya tidak berjalan
