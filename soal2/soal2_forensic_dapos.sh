#!/bin/bash
mkdir -p forensic_log_website_daffainfo_log

awk -F: '
NR == 1 { min = max = $3 } { if ($3 > max) max = $3; else if ($3 < min) min = $3 } END {print "Rata-rata serangan adalah sebanyak", (NR-1)/(max-min+1), "request per jam\n"} 
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

awk -F: '
NR>1 { gsub(/"/, "") } { if ( x[$1]++ >= max ) max = x[$1] } END { for (i in x) if(max == x[i]) print "IP yang paling banyak mengakses server adalah:", i, "sebanyak", x[i], "requests\n" } ' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

awk '/curl/ {i++} END {print "\nAda", i, "requests yang menggunakan curl sebagai user agent\n\n"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

awk -F: '{if($3=="02") print $1}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt



