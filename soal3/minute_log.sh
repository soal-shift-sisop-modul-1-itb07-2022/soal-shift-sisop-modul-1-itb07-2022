#!/bin/bash

free1=$(free -m | awk '/Mem:/ {print $2","$3","$4","$5","$6","$7}')
free2=$(free -m | awk '/Swap:/ {print $2","$3","$4}')
disk=$(du -sh /home/afrida/ | awk '{print $2","$1}')

echo $free1,$free2,$disk > /home/afrida/log/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log

#memastikan file log hanya dapat dibaca oleh user pemilik file
chmod 400 /home/afrida/log/metrics_$(date +\%Y\%m\%d\%H\%M\%S).log
