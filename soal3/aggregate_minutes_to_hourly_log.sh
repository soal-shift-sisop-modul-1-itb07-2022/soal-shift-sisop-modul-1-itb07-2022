#!/bin/bash

cd /home/afrida/log
cat metrics_2*.log> /home/afrida/SisOp/modul1/soal3/result.txt


#mencari nilai minimum setiap metrics
min_mem_total=$(awk -F, 'min=="" || $1 < min {min=$1} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_mem_used=$(awk -F, 'min=="" || $2 < min {min=$2} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_mem_free=$(awk -F, 'min=="" || $3 < min {min=$3} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_mem_shared=$(awk -F, 'min=="" || $4 < min {min=$4} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_mem_buff=$(awk -F, 'min=="" || $5 < min {min=$5} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_mem_available=$(awk -F, 'min=="" || $6 < min {min=$6} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_swap_total=$(awk -F, 'min=="" || $7 < min {min=$7} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_swap_used=$(awk -F, 'min=="" || $8 < min {min=$8} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_swap_free=$(awk -F, 'min=="" || $9 < min {min=$9} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)
min_path_size=$(awk -F, 'min=="" || $11 < min {min=$11} END {print min}' /home/afrida/SisOp/modul1/soal3/result.txt)


#mencari nilai maksimum setiap metrics
max_mem_total=$(awk -F, 'max=="" || $1 > max {max=$1} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_mem_used=$(awk -F, 'max=="" || $2 > max {max=$2} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_mem_free=$(awk -F, 'max=="" || $3 > max {max=$3} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_mem_shared=$(awk -F, 'max=="" || $4 > max {max=$4} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_mem_buff=$(awk -F, 'max=="" || $5 > max {max=$5} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_mem_available=$(awk -F, 'max=="" || $6 > max {max=$6} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_swap_total=$(awk -F, 'max=="" || $7 > max {max=$7} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_swap_used=$(awk -F, 'max=="" || $8 > max {max=$8} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_swap_free=$(awk -F, 'max=="" || $9 > max {max=$9} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)
max_path_size=$(awk -F, 'max=="" || $11 > max {max=$11} END {print max}' /home/afrida/SisOp/modul1/soal3/result.txt)

#mencari rata-rata dari setiap metrics
avg_mem_total=$(awk -F, '{ sum += $1 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_mem_used=$(awk -F, '{ sum += $2 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_mem_free=$(awk -F, '{ sum += $3 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_mem_shared=$(awk -F, '{ sum += $4 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_mem_buff=$(awk -F, '{ sum += $5 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_mem_available=$(awk -F, '{ sum += $6 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_swap_total=$(awk -F, '{ sum += $7 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_swap_used=$(awk -F, '{ sum += $8 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_swap_free=$(awk -F, '{ sum += $9 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)
avg_path_size=$(awk -F, '{ sum += $11 } END { print sum / NR }' /home/afrida/SisOp/modul1/soal3/result.txt)

#print semua
echo -e "minimum,$min_mem_total,$min_mem_used,$min_mem_free,$min_mem_shared,$min_mem_buff,$min_mem_available,$min_swap_total,$min_swap_used,$min_swap_free,/home/afrida/,$min_path_size\nmaximum,$max_mem_total,$max_mem_used,$max_mem_free,$max_mem_shared,$max_mem_buff,$max_mem_available,$max_swap_total,$max_swap_used,$max_swap_free,/home/afrida/,$max_path_size\naverage,$avg_mem_total,$avg_mem_used,$avg_mem_free,$avg_mem_shared,$avg_mem_buff,$avg_mem_available,$avg_swap_total,$avg_swap_used,$avg_swap_free,/home/afrida/,$avg_path_size" > /home/afrida/log/metrics_agg_$(date +\%Y\%m\%d\%H).log

#memastikan file log hanya dapat dibaca oleh user pemilik file
chown afrida /home/afrida/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
chmod 400 /home/afrida/log/metrics_agg_$(date +\%Y\%m\%d\%H).log
